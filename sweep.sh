#!/bin/bash

cd build;

file_name="../my_scenes/simple/teapot-area-light.pbrt"
out_name="teapot"


#
# from TIGHT to lose
#
 
#
# 0
#
sed -i '29s/.*/uint16_t exp_x = ((o_x >> 23) \& 0xff);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 15) \& 0xff);/' ../src/accelerators/predictor.cpp

make
./pbrt $file_name > $out_name"_0.txt"

#
# 1
#
sed -i '29s/.*/uint16_t exp_x = ((o_x >> 24) \& 0x7f);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 16) \& 0x7f);/' ../src/accelerators/predictor.cpp


make
./pbrt $file_name > $out_name"_1.txt"

#
# 2
#
sed -i '29s/.*/uint16_t exp_x = ((o_x >> 25) \& 0x3f);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 17) \& 0x3f);/' ../src/accelerators/predictor.cpp


make
./pbrt $file_name > $out_name"_2.txt"

#
# 3
#
sed -i '29s/.*/uint16_t exp_x = ((o_x >> 26) \& 0x1f);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 18) \& 0x1f);/' ../src/accelerators/predictor.cpp



make
./pbrt $file_name > $out_name"_3.txt"

#
# 4
#

sed -i '29s/.*/uint16_t exp_x = ((o_x >> 27) \& 0xf);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 19) \& 0xf);/' ../src/accelerators/predictor.cpp


make
./pbrt $file_name > $out_name"_4.txt"

#
# 5
#
sed -i '29s/.*/uint16_t exp_x = ((o_x >> 28) \& 0x7);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 20) \& 0x7);/' ../src/accelerators/predictor.cpp


make
./pbrt $file_name > $out_name"_5.txt"

#
# 6
#
sed -i '29s/.*/uint16_t exp_x = ((o_x >> 29) \& 0x3);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 21) \& 0x3);/' ../src/accelerators/predictor.cpp

make
./pbrt $file_name > $out_name"_6.txt"

# this one sucks!!
# 7
#

sed -i '29s/.*/uint16_t exp_x = ((o_x >> 30) \& 0x1);/' ../src/accelerators/predictor.cpp
sed -i '41s/.*/uint16_t mant_x = ((o_x >> 22) \& 0x1);/' ../src/accelerators/predictor.cpp

#make
#./pbrt $file_name > $out_name"_7.txt"
