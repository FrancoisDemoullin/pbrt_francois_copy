#!/bin/bash

cd build;

file_name="../my_scenes/sportscar/sportscar.pbrt"
out_name="sport_go_up_level"

#
# from TIGHT to lose
#
 
#
# 0
#
sed -i '28s/.*/const uint8_t go_up_level = 0;/' ../src/accelerators/predictor.h

make
./pbrt $file_name > $out_name"_0.txt"

#
# 1
#
sed -i '28s/.*/const uint8_t go_up_level = 1;/' ../src/accelerators/predictor.h


make
./pbrt $file_name > $out_name"_1.txt"

#
# 2
#
sed -i '28s/.*/const uint8_t go_up_level = 2;/' ../src/accelerators/predictor.h


make
./pbrt $file_name > $out_name"_2.txt"

#
# 3
#
sed -i '28s/.*/const uint8_t go_up_level = 3;/' ../src/accelerators/predictor.h



make
./pbrt $file_name > $out_name"_3.txt"

#
# 4
#

sed -i '28s/.*/const uint8_t go_up_level = 4;/' ../src/accelerators/predictor.h

make
./pbrt $file_name > $out_name"_4.txt"

#
# 5
#
sed -i '28s/.*/const uint8_t go_up_level = 5;/' ../src/accelerators/predictor.h


make
./pbrt $file_name > $out_name"_5.txt"

#
# 6
#
sed -i '28s/.*/const uint8_t go_up_level = 6;/' ../src/accelerators/predictor.h

make
./pbrt $file_name > $out_name"_6.txt"

