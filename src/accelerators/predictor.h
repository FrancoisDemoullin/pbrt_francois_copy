#ifndef PBRT_ACCELERATORS_PREDICTOR_H
#define PBRT_ACCELERATORS_PREDICTOR_H

// accelerators/predictor.h*
#include "pbrt.h"
#include <unordered_map>
#include <set>
#include <mutex> 
#include <atomic>

namespace pbrt {

class Predictor {
  public:
    // Predictor Public Methods
    Predictor();
    ~Predictor();

    // Training
    void train(const Ray &ray, const std::set<int> &hit_nodes);

    // Testing
    bool lookup(const Ray &ray, std::set<int> &leaf_set);
    bool lookupP(const Ray &ray);
    void increment_stats(const bool hit_any, const bool predictor_hit, const bool verify);
    void incrment_verify(const bool hit_any);
    void increment_ground_truth(const bool hit_any, const bool predictor_hit);

    // Utility
    void print_stats() const;

    // Properties
    const uint8_t go_up_level = 5;
    const uint16_t number_of_entries_cap = 5;
    const bool use_replacement_policy = true;
    const uint entry_threshold = 128;

    // Lookup stats
    unsigned long num_lookups = 0;
    unsigned long predictor_hit_count = 0;
    
    // Verify stats
    unsigned long verifications_made = 0;
    std::atomic<unsigned long> new_intersection_counter{0};
    std::atomic<unsigned long> new_intersection_counter_P{0};
    unsigned long new_intersection_counter_P_verify = 0;
    unsigned long new_intersection_counter_P_reference = 0;
    unsigned long num_verified_predictions = 0;

    // Ground truth stats
    unsigned long ground_truths_made;
    std::atomic<unsigned long> reference_intersection_counter_P{0};
    std::atomic<unsigned long> reference_intersection_counter{0};

    // training stats
    unsigned long num_calls_to_train = 0;
    unsigned long num_new_entries = 0;
    unsigned long num_evictions_cap = 0;
    unsigned long early_rejections_due_to_cap = 0;
    unsigned long num_updates = 0;

    // eviction policy stats
    unsigned long num_evictions_LRU = 0;
    unsigned long num_updates_LRU = 0;

    // mutexes
    std::mutex predictor_mutex;
    std::mutex stats_mutex;

  private:
    // Predictor Private Methods 
    unsigned long long compute_hash(const Ray &ray);

    // Replacement/Eviction/Insertion policy
    void evict_entry();
    void update_LRU_table(unsigned long long hash);

    // Predictor stats
    unsigned int num_entries;
    std::unordered_map<unsigned long long, std::set<int>> predictor_table;
    std::unordered_map<unsigned long long, int> hit_histogram;
    std::unordered_map<unsigned long long, uint> LRU_table;   


    // unit tests - for peace of mind
    void unit_tests();
};

}  // namespace pbrt

#endif  // PBRT_ACCELERATORS_PREDICTOR_H