#include "accelerators/predictor.h"
#include "interaction.h"
#include "stats.h"
#include <algorithm>
#include <math.h>
#include <assert.h>
#include <set>


namespace pbrt {

  uint16_t hash_float(float x) {
    uint32_t o_x = *( (uint32_t*) &x );

    // take most significant num_digits of position
    bool sign_bit_x = (o_x >> 31) & 0x1;

    // 30, 0x1 = 1 bit
    // 29, 0x3 = 2 bits
    // 28, 0x7 = 3 bits
    // 27, 0xf = 4 bits

    // won't fit into long
    // 26, 0x1f = 5 bits
    // 25, 0x3f = 6 bits
    // 24, 0x7f = 7 bits
    uint16_t exp_x = ((o_x >> 26) & 0x1f);

    // 22, 0x1 = 1 bit
    // 21, 0x3 = 2 bits
    // 20, 0x7 = 3 bits
    // 19, 0xf = 4 bits

    // won't fit into long
    // 18, 0x1f = 5 bits
    // 17, 0x3f = 6 bits
    // 16, 0x7f = 7 bits
    // 15, 0xff = 8 bits
    uint16_t mant_x = ((o_x >> 18) & 0x1f);
    
    // make sure it all fits!
    uint16_t hash_x = (sign_bit_x << 15) | (exp_x << 7) |  mant_x;

    return hash_x;
    // return 0;
  }


  Predictor::Predictor()
  {
    predictor_table = {};
    LRU_table = {};
    hit_histogram = {};

    num_entries = 0;

    unit_tests();

    // Lookup stats
    num_lookups = 0;
    predictor_hit_count = 0;
    
    // Verify stats
    verifications_made = 0;
    // new_intersection_counter = 0;
    // new_intersection_counter_P = 0;

    // Ground truth stats
    ground_truths_made = 0;
    reference_intersection_counter = 0;
    reference_intersection_counter_P = 0;

    // training stats
    num_calls_to_train = 0;
    num_new_entries = 0;
    num_evictions_cap= 0;
    num_updates = 0;

    // eviction policy
    num_evictions_LRU = 0;

    //
    assert(predictor_table.size() == LRU_table.size());
    assert(num_entries == LRU_table.size());

  }

  Predictor::~Predictor()
  {

    print_stats();
    
    /*
    for (auto it : predictor_table)
    {
      printf("hash: %lx elements: ", it.first);
      for (auto set_it : it.second)
      {
        std::cout << set_it << " "; 
      }
      std::cout << std::endl;
    }
    */

    // do the cleanup + memory freeing
    predictor_table.clear();
  }

  void Predictor::print_stats() const
  {
    // error checking
    assert(num_entries == predictor_table.size());
    if (use_replacement_policy)
    {
      assert(num_entries == LRU_table.size());
    }

    // average number of entries in set
    unsigned int leaf_counter = 0;
    unsigned int max_leaf_count = 0;
    for (auto it : predictor_table) {
      unsigned int cur_leaf_count = it.second.size();

      assert(it.second.size() >= 1);
      
      leaf_counter += cur_leaf_count;
      max_leaf_count = std::max(cur_leaf_count, max_leaf_count);
    }
    float average_num_leafs = (float)((float)leaf_counter / (float)num_entries);

    // get size of the table + tag!
    unsigned int byte_size = 
       predictor_table.size() * sizeof(unsigned int) * sizeof(average_num_leafs) // data
     + predictor_table.size() * sizeof(unsigned long long); // tags
    float kb_size = (float)byte_size / 1024.f;
    float mb_size = (float)byte_size / (1024.f * 1024.f);
    float gb_size = (float)byte_size / (1024.f * 1024.f * 1024.f);

    // hit histogram
    const uint hit_histogram_counter_size = 100;
    int hit_histogram_counter[hit_histogram_counter_size] = {0};

    for (auto it : hit_histogram) {

      int index = it.second;
      if (index >= hit_histogram_counter_size) {
        index = hit_histogram_counter_size - 1;
      }
      hit_histogram_counter[index]++;
    }

    // print the predictor stats
    std::cout << "ray stats: " << " \n"
    << "0. General info about predictor: " << " \n"
    << "num_entries: " << num_entries <<  " \n"
    << "average number of leafs per entry: " << average_num_leafs << " \n"
    << "max_leaf_count: " << max_leaf_count << " \n"
    << "byte_size: " << byte_size << " bytes " << kb_size << "kB " << mb_size << "MB " << gb_size << "GB \n"
    << "go_up_level" << go_up_level << " \n"
    << "number_of_entries_cap " << number_of_entries_cap << " \n"
    << "entry_threshold " << entry_threshold << " \n"
    << "use_replacement_policy " << use_replacement_policy << " \n"
    << " \n"

    << "1. lookup stage" << " \n"
    << "num_lookups: " << num_lookups << " \n"
    << "predictor_hit_count: " << predictor_hit_count << " \n"
    << " \n"

    << "2. verify stage" << " \n"
    << "verifications_made: " << verifications_made << " \n"
    << "num_verified_predictions: " << num_verified_predictions << " \n"
    << "new_intersection_counter: " << new_intersection_counter << " \n"
    << "new_intersection_counter_P: " << new_intersection_counter_P << " \n"
    << "new_intersection_counter_P_verify: " << new_intersection_counter_P_verify << " \n"
    << "new_intersection_counter_P_reference: " << new_intersection_counter_P_reference << " \n"
    << " \n"

    << "3. ground truth stage" << " \n"
    << "ground_truths_made: " << ground_truths_made << " \n"
    << "reference_intersection_counter: " << reference_intersection_counter << " \n"
    << "reference_intersection_counter_P: " << reference_intersection_counter_P << " \n"
    << " \n"

    << "4. training stage" << " \n"
    << "num_calls_to_train: " << num_calls_to_train << " \n"
    << "num_new_entries: " << num_new_entries << " \n"
    << "early_rejections_due_to_cap: " << early_rejections_due_to_cap << " \n"
    << "num_evictions_cap: " << num_evictions_cap<< " \n"
    << "num_updates: " << num_updates<< " \n"
    << " \n"

    << "5. Savings" << " \n"    
    << "intersection savings: " << 1.f - (static_cast<float>(new_intersection_counter) / static_cast<float>(reference_intersection_counter)) << " \n"
    << "intersection savings P: " << 1.f - (static_cast<float>(new_intersection_counter_P) / static_cast<float>(reference_intersection_counter_P)) << " \n"
    << " \n"

    << "6. Eviction Policy" << "\n"
    << "num_updates_LRU: " << num_updates_LRU << " \n"
    << "num_evictions_LRU: " << num_evictions_LRU << " \n" 
    << "use_replacement_policy" << use_replacement_policy << " \n"

    << "7. Hit Histogram" << "\n"
    << std::endl;

    for (int i = 0; i < hit_histogram_counter_size; i++)
    {
      std::cout << hit_histogram_counter[i] << " ";
    }
    std::cout << std::endl;
  }

  unsigned long long Predictor::compute_hash(const Ray &ray)
  { 

    // ProfilePhase p(Prof::PredictorHash);

    unsigned long long hash_o_x = static_cast<unsigned long long>(hash_float(ray.o.x));
    unsigned long long hash_o_y = static_cast<unsigned long long>(hash_float(ray.o.y));
    unsigned long long hash_o_z = static_cast<unsigned long long>(hash_float(ray.o.z));
    unsigned long long hash_d_x = static_cast<unsigned long long>(hash_float(ray.d.x));
    unsigned long long hash_d_y = static_cast<unsigned long long>(hash_float(ray.d.y));
    unsigned long long hash_d_z = static_cast<unsigned long long>(hash_float(ray.d.z));

    // unsigned long long can fit 8 bytes = 8 * uint8_t = 4 * uint16_t

    unsigned long long hash_0 = hash_o_x ^ hash_d_z;
    unsigned long long hash_1 = hash_o_y ^ hash_d_y;
    unsigned long long hash_2 = hash_o_z ^ hash_d_x;

    unsigned long long hash = (hash_0 << 0) |
                              (hash_1 << 16) |
                              (hash_2 << 32);

    return hash; 
  }

  void Predictor::evict_entry()
  {
    assert(predictor_table.size() == LRU_table.size()); 
    assert(LRU_table.size() == num_entries);

    if (predictor_table.size() == entry_threshold)
    {
      // find max entry in LRU table
      uint max_duration = 0;
      unsigned long long max_hash = 0;

      for(auto entry_it : LRU_table)
      {
        if (entry_it.second >= max_duration)
        {
          max_hash = entry_it.first;
          max_duration = entry_it.second;
        }
      }

      // evict
      auto to_be_evicted = predictor_table.find(max_hash);
      if (to_be_evicted == predictor_table.end())
      {
        assert(to_be_evicted != predictor_table.end());
      }

      predictor_table.erase(to_be_evicted);
      LRU_table.erase(LRU_table.find(max_hash));
      num_entries--;
      num_evictions_LRU++;
    }

    assert(predictor_table.size() == LRU_table.size());
    assert(LRU_table.size() == num_entries);
  }

  void Predictor::update_LRU_table(unsigned long long hash)
  { 
    num_updates_LRU++;
    bool has_been_reset_sanity = false;

    for(auto entry_it : LRU_table)
    {
      if (entry_it.first == hash)
      {
        entry_it.second = 0;
        has_been_reset_sanity = true;
      } else {
        entry_it.second++;
      } 
    }
    // error checking
    assert(has_been_reset_sanity);
  }


  // hit all -> pass list of intersections via leaf_set
  bool Predictor::lookup(const Ray &ray, std::set<int> &leaf_set)
  {
    this->num_lookups++;

    unsigned long long hash = compute_hash(ray);

    // ProfilePhase p(Prof::PredictorLookup);

    // guard against race conditions
    // std::lock_guard<std::mutex> lck (predictor_mutex);

    // check if hash in table
    std::unordered_map<unsigned long long,std::set<int>>::iterator got = predictor_table.find(hash);
    
    if (got == predictor_table.end())
    {
      // predictor miss
      return false;
    } else {
      // predictor hit: pass set via args
      leaf_set = got->second;
      this->predictor_hit_count++;
      // update the LRU
      if (use_replacement_policy)
      {
        update_LRU_table(hash);
      }
    }
    return true;
  }

  void Predictor::incrment_verify(const bool hit_any)
  {
    if (hit_any)
    {
      new_intersection_counter_P++;
      new_intersection_counter_P_verify++;
    } else {
      new_intersection_counter++;
    }
  }

  void Predictor::increment_ground_truth(const bool hit_any, const bool predictor_hit)
  {
    // if we are doing the full traversal
    // only add these counters to the "new_" counters if there was a miss in the predictor
    // this is the penalty for an incorrect prediction!! or a miss in the predictor
    if (hit_any)
    {
      reference_intersection_counter_P++;
      if (!predictor_hit)
      {
        new_intersection_counter_P++;
        new_intersection_counter_P_reference++;
      }
    } else {
      reference_intersection_counter++;
      if (!predictor_hit)
      {
        new_intersection_counter++;
      }
    }
  }

  void Predictor::increment_stats(const bool hit_any, const bool predictor_hit, const bool verify)
  {
    if (verify)
    {
      incrment_verify(hit_any);
    } else {
      increment_ground_truth(hit_any, predictor_hit);
    }
  }
  
  
  void Predictor::train(const Ray &ray, const std::set<int> &hit_nodes)
  { 
    num_calls_to_train++;

    // cap policy - on insert
    if (hit_nodes.size() >= this->number_of_entries_cap)
    {
      this->early_rejections_due_to_cap++;
      return;
    }

    unsigned long long hash = compute_hash(ray);
      
    // guard against race conditions
    std::lock_guard<std::mutex> lck (predictor_mutex);

    // check if hash in table
    std::unordered_map<unsigned long long,std::set<int>>::iterator got = predictor_table.find(hash);

    if (got == predictor_table.end())
    {
      // evict some entry if necessary
      if (use_replacement_policy)
      {
        evict_entry();
      }

      // add hash to map
      predictor_table[hash] = hit_nodes;
      hit_histogram[hash] = 0;
      if (use_replacement_policy)
      {
        LRU_table[hash] = 0;  
      }

      num_entries++;
      num_new_entries++;

    } else {
      // hash was in predictor, add leaf_index if it is not yet in this hash's set
      std::set<int> *prediction_set = &(got->second);

      // increment the hit histogram
      std::unordered_map<unsigned long long,int>::iterator hit_hist_it = hit_histogram.find(hash);
      assert(hit_hist_it != hit_histogram.end());
      hit_hist_it->second++;

      // insert the node
      for (auto hit_node : hit_nodes)
      {
        prediction_set->insert(hit_node);  
      }

      // cap policy - on update
      if (prediction_set->size() >= this->number_of_entries_cap)
      {
        // evict from predictor table
        predictor_table.erase(got);

        // evict from LRU table
        if (use_replacement_policy)
        {
          auto got_LRU = LRU_table.find(hash);
          assert(got_LRU != LRU_table.end());
          LRU_table.erase(got_LRU);
        }

        num_entries--;
        num_evictions_cap++;

        return;
      }
      
      // update was succesful
      num_updates++;
      // update_LRU_table(hash); // TODO! Should this be here?
    }

  }

  // use GDB here!
  // p/t on the hex values for visual unit test of the hash function
  void Predictor::unit_tests()
  {
    // testing the hash function
    float test = 0.1f;
    uint16_t hash_coutput = hash_float(test);
  }
}



/* OBSOLETE

  unsigned long Predictor::compute_hash(const Ray &ray)
  { 

    ProfilePhase p(Prof::PredictorHash);

    // POSITION
    uint8_t num_digits = global_o_displacement + origin_precision; 

    // clamp origin
    float o_x = std::min(9.99f, std::abs(ray.o.x));
    float o_y = std::min(9.99f, std::abs(ray.o.y));
    float o_z = std::min(9.99f, std::abs(ray.o.z));

    // take most significant num_digits of position
    unsigned int o_x_i = int(o_x * pow(10, origin_precision));
    unsigned int o_y_i = int(o_y * pow(10, origin_precision));
    unsigned int o_z_i = int(o_z * pow(10, origin_precision));

    // displace 
    unsigned int o_z_displaced = o_z_i * pow(10, (0 * (num_digits)));
    unsigned int o_y_displaced = o_y_i * pow(10, (1 * (num_digits)));
    unsigned int o_x_displaced = o_x_i * pow(10, (2 * (num_digits)));

    // AND
    unsigned int o_hash = o_x_displaced + o_y_displaced + o_z_displaced;
    assert(o_hash < 4294967295); // check for upper limit on unsigned int

    // DIRECTION
    num_digits = global_d_displacement + direction_precision; 

    float d_x = std::abs(ray.d.x);
    float d_y = std::abs(ray.d.y);
    float d_z = std::abs(ray.d.z);

    // take most significant num_digits of position
    unsigned int d_x_i = int(d_x * pow(10, direction_precision));
    unsigned int d_y_i = int(d_y * pow(10, direction_precision));
    unsigned int d_z_i = int(d_z * pow(10, direction_precision));

    // displace 
    unsigned int d_z_displaced = d_z_i * pow(10, (0 * (num_digits)));
    unsigned int d_y_displaced = d_y_i * pow(10, (1 * (num_digits)));
    unsigned int d_x_displaced = d_x_i * pow(10, (2 * (num_digits)));

    unsigned int d_hash = d_x_displaced + d_y_displaced + d_z_displaced;
    assert(d_hash < 4294967295); // check for upper limit on unsigned int

    unsigned long o_hash_displace = o_hash * pow(10, (3 * num_digits));
    unsigned long hash = o_hash_displace + d_hash; 

  
    std::cout << ray.o.x << " " << ray.o.y << " " << ray.o.z << " " << ray.d.x << " " << ray.d.y << " " << ray.d.z << std::endl;
    std::cout << "o_hash: " << o_hash << std::endl;
    std::cout << "d_hash: " << d_hash << std::endl;
    std::cout << "hash: " << hash << std::endl;
  

    return hash; 
  }
*/